/*
 * Copyright (C) 2018 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.aboutme

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.android.aboutme.databinding.ActivityMainBinding
import com.example.android.aboutme.model.User
import com.jakewharton.rxbinding.view.RxView
import io.reactivex.subjects.PublishSubject
import rx.Observable


/**
 * Main Activity of the AboutMe app. This app demonstrates:
 *     * LinearLayout with TextViews, ImageView, Button, EditText, and ScrollView
 *     * ScrollView to display scrollable text
 *     * Getting user input with an EditText.
 *     * Click handler for a Button to retrieve text from an EditText and set it in a TextView.
 *     * Setting the visibility status of a view.
 *     * Data binding between MainActivity and activity_main.xml. How to remove findViewById,
 *       and how to display data in views using the data binding object.
 *       It is also using Rxjava later.
 */
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val user: User = User("Varun Singh")
    var buttonRx: PublishSubject<View> = PublishSubject.create()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_main)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.myuser = user
        return with(binding)
        {
            //without rxjava
            /*binding.button.setOnClickListener { addNickNamewa(it) }
            root*/
            //Rxjava binding jake wharton rxview
        /*    RxView.clicks(button).subscribe {
                //handle on click here
                addNickNamewa(root)
            }*/

            binding.button.setOnClickListener {v-> buttonRx.onNext(addNickNamewa(root))}
        }

        //Old methods without binding
      /*  findViewById<Button>(R.id.button).setOnClickListener {
            addNickNamewa(it)
        }

      */


    }

/*  //if you want to use Unit so no retrun type
    private fun <T> PublishSubject<T>.onNext(addNickNamewa: Unit) {
        //return addNickNamewa

    }*/
    // if you want to use Any with return statement
    private fun <T> PublishSubject<T>.onNext(addNickNamewa: Any): Any {
        return addNickNamewa
    }



    private fun addNickNamewa(view: View)
    {
       /* val nicknameEdittext = findViewById<EditText>(R.id.nickname_edit)
        val nicknameText = findViewById<TextView>(R.id.nickname_text)*/

/*        //text of nickname set text to edittext text using get text
        nicknameText.text = nicknameEdittext.text
        nicknameEdittext.visibility = View.GONE
        view.visibility = View.GONE
        nicknameText.visibility = View.VISIBLE*/

      binding.apply {
           // nicknameText.text = nicknameEdit.text
          /*after binding*/
            myuser?.nickname = nicknameEdit.text.toString()
            invalidateAll()
            nicknameEdit.visibility = View.GONE
            button.visibility = View.GONE
            nicknameText.visibility = View.VISIBLE
        }
/*

        binding.nicknameText.text = binding.nicknameEdit.text
        binding.nicknameEdit.visibility = View.GONE
        binding.button.visibility = View.GONE
        binding.nicknameText.visibility = View.VISIBLE
*/

        //Hide the keyboard
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}

